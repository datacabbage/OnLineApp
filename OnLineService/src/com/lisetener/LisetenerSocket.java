package com.lisetener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.socket.SocketServer;



@WebListener
public class LisetenerSocket implements ServletContextListener {
    public LisetenerSocket() {
      
    }


    public void contextDestroyed(ServletContextEvent arg0)  { 
         
    }

    public void contextInitialized(ServletContextEvent arg0)  { 
    	new Thread(new Runnable() {
			
			@Override
			public void run() {
		        // ���������
				SocketServer socketServer = new SocketServer();
				socketServer.startServer();
			}
		}).start();
    }

	
}
